import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Calculator {

    @Test
    public void testCalculator() throws MalformedURLException, InterruptedException {

        //Desired Capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("platformVersion","11");
        capabilities.setCapability("appActivity","com.android.calculator2.Calculator");
        capabilities.setCapability("appPackage","com.google.android.calculator");

        AndroidDriver<AndroidElement> driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Suma 2 numeros enteros
        System.out.println("Resultado de la suma: "+operacion(6,7,'S',driver));

        //Resta 2 numeros enteros
        System.out.println("Resultado de la resta: "+operacion(8,4,'R',driver));

        //Multiplicacion 2 numeros decimales
        System.out.println("Resultado de la mult: "+operacion(3,9,'M',driver));

        //Division 2 numeros decimales
        System.out.println("Resultado de la div: "+operacion(9,3,'D',driver));

        driver.quit();
    }

    public String operacion(int num1, int num2, char op, AndroidDriver driver){

        driver.findElementById("com.google.android.calculator:id/digit_"+num1).click();

        List<AndroidElement> lista = driver.findElements(By.id("com.google.android.calculator:id/digit_1"));
        lista.get(0).click();

        if (op == 'S'){
            driver.findElement(new MobileBy.ByAccessibilityId("plus")).click();
        }
        if (op == 'R'){
            driver.findElement(new MobileBy.ByAccessibilityId("minus")).click();
        }
        if (op == 'M'){
            driver.findElement(new MobileBy.ByAccessibilityId("multiply")).click();
        }
        if (op == 'D'){
            driver.findElement(new MobileBy.ByAccessibilityId("divide")).click();
        }

        driver.findElementById("com.google.android.calculator:id/digit_"+num2).click();

        driver.findElement(By.xpath("//android.widget.Button[@content-desc=\"equals\"]")).click();

        return driver.findElementById("com.google.android.calculator:id/result_final").getText();
    }
}
