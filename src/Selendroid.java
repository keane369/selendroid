import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Selendroid {

    @Test
    public void testSelendroidApp() throws MalformedURLException, InterruptedException {

        //Desired Capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName","Android");
        capabilities.setCapability("platformVersion","11");
        capabilities.setCapability("appActivity","io.selendroid.testapp.HomeScreenActivity");
        capabilities.setCapability("appPackage","io.selendroid.testapp");
        capabilities.setCapability("app","/Users/ivanrivas/Documents/Personal/Appium Training/Sep2020/CalculadoraAppium/src/selendroid-test-app.apk");

        AndroidDriver<AndroidElement> driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Click on continue button
        System.out.println("Trying to click continue button...");
        driver.findElement(By.id("com.android.permissioncontroller:id/continue_button")).click();
        System.out.println("Continue button clicked!");

        //Verify pop-up
        System.out.println("Validating if alert is present...");
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElementById("android:id/alertTitle").isDisplayed(),"PopUp is not present");
        System.out.println("Alert is present!");

        //Dismiss pop-up
        driver.findElement(By.id("android:id/button1")).click();
        System.out.println("PopUp Dismissed");

        //Check check-box and verify
        driver.findElementById("io.selendroid.testapp:id/input_adds_check_box").click();
        System.out.println("Checkbox clicked!");

        //System.out.println("Boolean value: "+driver.findElementById("io.selendroid.testapp:id/input_adds_check_box").getAttribute("checked"));
        Boolean checked = Boolean.parseBoolean(driver.findElementById("io.selendroid.testapp:id/input_adds_check_box").getAttribute("checked"));
        Assert.assertFalse(checked, "Valor no esta seleccionado");
        System.out.println("Checkbox verified!");

        //Dismiss pop-up
        driver.findElementById("io.selendroid.testapp:id/showPopupWindowButton").click();
        Assert.assertTrue(driver.findElementById("io.selendroid.testapp:id/showPopupWindowButton").isDisplayed(),"Alert is not displayed");
        Thread.sleep(2000);
        driver.findElementById("io.selendroid.testapp:id/showPopupWindowButton").click();
        System.out.println("PopUp dismissed");

        //Send Text
        driver.findElementByAccessibilityId("my_text_fieldCD").sendKeys("Hola a todos");
        System.out.println("Text sent!");

        //Verify Text
        driver.findElementById("io.selendroid.testapp:id/visibleButtonTest").click();
        Assert.assertTrue(driver.findElementById("io.selendroid.testapp:id/visibleTextView").isDisplayed(),"Text is not displayed");
        System.out.println("Text is Displayed!");

        driver.findElementById("io.selendroid.testapp:id/visibleButtonTest").click();
        Assert.assertFalse(driver.findElementsById("io.selendroid.testapp:id/visibleTextView").size()>0,"Text is displayed");
        System.out.println("Text is not displayed!");

        //Show progress bar
        driver.findElement(By.id("io.selendroid.testapp:id/waitingButtonTest")).click();
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.id("android:id/progress")).isDisplayed(),"Progress bar is not displayed");
        System.out.println("Progress bar displayed!");

        Thread.sleep(15000);
        Assert.assertTrue(driver.findElementByAccessibilityId("label_usernameCD").isDisplayed(),"Progress bar is not displayed");
        System.out.println("New screen displayed!");

        //Tap back
        driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.pressKeyCode(AndroidKeyCode.BACK);
        System.out.println("Home screen displayed");

        driver.quit();
    }
}
